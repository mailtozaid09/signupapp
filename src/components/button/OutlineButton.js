import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';
const {width} = Dimensions.get('window');

const OutlineButton = (props) => {

    const { title, onPress } = props; 
    
    return (
        <View style={styles.container} >
            <Text style={styles.uploadTitle} >UPLOAD IDENTITY DOCUMENT</Text> 
            <TouchableOpacity activeOpacity={0.4} onPress={onPress} style={styles.button} >
                <Image source={require('../../../assets/upload.png')} style={styles.icon} />
                <Text style={styles.buttonText} >{title}</Text>
            </TouchableOpacity>
        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 20,
    },
    button: {
        width: '100%',
        height: 50,
        borderRadius: 25,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: Colors.WHITE,
        backgroundColor: Colors.ORANGE
    },
    buttonText: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        color: Colors.WHITE
    },
    icon: {
        height: 22,
        width: 22,
        marginRight: 20,
    },
    uploadTitle: {
        fontSize: 14,
        lineHeight: 22,
        fontWeight: 'bold',
        color: Colors.WHITE
    }
})


export default OutlineButton