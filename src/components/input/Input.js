import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';

const {width} = Dimensions.get('window');

const Input = (props) => {

    const { title, placeholder, value, onChangeText, icon, } = props;
    
    return (
        <View style={styles.container} >
            <Text style={styles.inputTitle}>{title}</Text>
            <View style={styles.inputContainer}>
                <Image source={icon} style={styles.icon} />
                <TextInput 
                    value={value}
                    placeholder={placeholder}
                    onChangeText={onChangeText}
                    style={styles.inputStyle}
                    placeholderTextColor={Colors.CREAM}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        
    },
    inputTitle: {
        fontSize: 14,
        lineHeight: 16,
        marginTop: 15,
        color: Colors.WHITE,
    },
    inputContainer: {
        marginTop: 10,
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 6,
        backgroundColor: Colors.LIGHT_ORANGE
    },
    inputStyle: {
        height: 40,
        color: Colors.WHITE,
    },
    icon: {
        height: 22,
        width: 22,
        marginRight: 6,
    }
})

export default Input