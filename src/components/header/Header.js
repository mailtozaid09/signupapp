import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, ScrollView , TouchableOpacity } from 'react-native';
import Colors from '../../style/Colors';
import Input from '../input/Input';

const {width} = Dimensions.get('window');

const Header = () => {
    
    return (
        <View>
            <View style={styles.background} >
                <View style={styles.headerContainer} >
                    
                    <Text style={styles.headerTitle} >SIGN UP</Text>
                    <Image source={require('../../../assets/logo.jpg')} style={styles.icon} />
                </View>
                
            </View>
            <TouchableOpacity  style={styles.backArrow}>
                <Image source={require('../../../assets/left-arrow.png')} style={styles.arrow} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 22,
        color: Colors.WHITE,
    },
    background: {
        height: 200,
        width: 400, 
        top: -35,
        left: -18,
        position: 'absolute',
        alignItems: 'center',
        borderBottomLeftRadius: 200,
        borderBottomRightRadius: 200,
        backgroundColor: Colors.WHITE,
    },
    headerContainer: {
        width: 120,
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
    },
    headerTitle: {
        marginTop: 50,
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 22,
        color: Colors.BLACK,
    },
    icon: {
        height: 100,
        width: 100,
        marginTop: 15,
        resizeMode: 'contain'
    },
    backArrow: {
        position: 'absolute', 
        top: 15, 
        left: 20, 
    },
    arrow: {
        height: 22,
        width: 22
    }
})

export default Header