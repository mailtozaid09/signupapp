import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, TextInput, ScrollView , TouchableOpacity } from 'react-native';
import Button from '../components/button/Button';
import OutlineButton from '../components/button/OutlineButton';
import Header from '../components/header/Header';
import Input from '../components/input/Input';
import Colors from '../style/Colors';

const {width} = Dimensions.get('window');

const SignUpScreen = () => {

    const [name, setName] = useState('');
    const [buisnessName, setbuisnessName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState('');
    const [address, setAddress] = useState("");
    const [error, setError] = useState('');


    const LoginText = () => {
        return(
            <View style={styles.loginContainer} >
                <Text style={styles.account} >Already have account?</Text>
                <TouchableOpacity>
                    <Text style={styles.login} >Log In</Text>
                </TouchableOpacity>
            </View>
        )
    }

    const ErrorText = () => {
        return(
            <View style={styles.errorContainer} >
                <Text style={styles.errorText}>{error}</Text>
            </View>
                    
        )
    }

    const saveDetails = () => {
    
        if(name !== ''){
            if(buisnessName !== ''){
                if(phoneNumber !== ''){
                    if(address !== ''){
                        alert("Details Saved " + "\n" + "Name : " + name + "\n" +  "Buisness Name : " + buisnessName + "\n" +  "Number : " + phoneNumber + "\n" +  "Address : " + address)
                    }else{
                        setError('Address can\'t be blank');
                        console.log('---Address: ' + address);
                    }
                }else{
                    setError('Number can\'t be blank');
                    console.log('---phoneNumber: ' + phoneNumber);
                }
            }else{
                setError('Buisness Name can\'t be blank');
                console.log('---buisnessName: ' + buisnessName);
            }
        }else{
            setError('Name can\'t be blank');
            console.log('---name: ' + name);
        }
    }

    

    return (
        <View style={styles.mainContainer} >
            <ScrollView keyboardShouldPersistTaps='always' keyboardDismissMode="on-drag">
                <Header />

                <View style={styles.container} >
                    <Input
                        title="NAME"
                        value={name}
                        onChangeText={(text) => { setError(''); setName(text); }}
                        placeholder="Name"
                        icon={require('../../assets/number.png')}
                    />

                    <Input
                        title="BUISNESS NAME"
                        value={buisnessName}
                        onChangeText={(text) => { setError(''); setbuisnessName(text)}}
                        placeholder="Buisness Name"
                        icon={require('../../assets/buisness.png')}
                    />

                    <Input
                        title="PHONE"
                        value={phoneNumber}
                        onChangeText={(text) => { setError(''); setPhoneNumber(text); }}
                        placeholder="Phone"
                        icon={require('../../assets/user.png')}
                    />

                    <Input
                        title="ADDRESS"
                        value={address}
                        onChangeText={(text) => { setError(''); setAddress(text); }}
                        placeholder="Address"
                        icon={require('../../assets/address.png')}
                    /> 

                    <OutlineButton
                        title='UPLOAD FILE'
                        onPress={() => {}}
                    />

                    
                    <Button
                        title='NEXT'
                        onPress={() => {saveDetails();}}
                    />

                    <ErrorText/>
                    
                    <LoginText/> 
                </View>
            </ScrollView>
          
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.ORANGE
    },
    container: {
        padding: 20,
        marginTop: 170,
    },
    account: {
        fontSize: 14,
        color: Colors.WHITE
    },
    login: {
        fontSize: 16,
        marginLeft: 4,
        color: Colors.WHITE,
        textDecorationLine: "underline",
        textDecorationStyle: "solid",
        textDecorationColor: "#000",
    },
    loginContainer: {
        marginTop: 10,
        marginBottom: 20,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    title: {
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 22,
        color: Colors.WHITE,
    },
    errorContainer: {
        alignItems: 'center'
    },
    errorText: {
        fontSize: 16,
        lineHeight: 22,
        marginTop: 5,
        color: Colors.RED,
    },
})

export default SignUpScreen